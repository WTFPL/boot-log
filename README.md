日志输出目录自动匹配项目名称,比如： 项目名称叫`demo-app`那么日志文件将输出到 `log/demo-app/`

本项目中的日志配置文件来自:https://gitee.com/log4j/pig,可以作为团队的通用配置,新建项目直接Copy就行了。


see:

- [Spring Boot 项目访问Maven项目属性](http://note.eta.pub/2019/09/04/boot-maven/)
- [spring-boot doc](https://docs.spring.io/spring-boot/docs/2.1.1.RELEASE/reference/html/boot-features-logging.html)